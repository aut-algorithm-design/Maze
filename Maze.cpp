// In the name of Allah
#include<bits/stdc++.h>
#define MAXN 100
#define MAXM 100


using namespace std;

struct primary_edge{
    int s;
    int f;
    char c;
};
/// first  -> Captain Rocket
/// second -> Lieutenant Lucky

int n;
int m;
char node_color[MAXN];
struct primary_edge edges[MAXM];
vector<int> primary_edge_node[MAXN];
int st1,st2;
vector<pair<int,char> > adjacent_list[MAXN*MAXN];
bool valid[MAXN*MAXN];
pair<int,char> parent[MAXN*MAXN];
int goal;

bool is_cross(int s,int f,char color_controled){
    for(int i=0 ; i<primary_edge_node[s].size() ; i++){
        if(edges[primary_edge_node[s][i]].f == f && edges[primary_edge_node[s][i]].c ==color_controled ){
            return true;
        }
    }
    return false;
}

bool is_goal(int node){
    if(node/(n+1)==n || node%(n+1)==n){
        return true;
    }
    else{
        return false;
    }
}

void BFS(){
    int start_BFS = st1*(n+1) + st2;
    queue<int> BFS_queue;
    BFS_queue.push(start_BFS);
    valid[start_BFS] = true;
    bool flag =true;
    while(BFS_queue.size() && flag){
        int top = BFS_queue.front();
        BFS_queue.pop();
        for(int i=0 ; i<adjacent_list[top].size() ; i++){
            int new_node = adjacent_list[top][i].first ;
            if(valid[new_node]==false){
                valid[new_node] = true;
                parent[new_node] = make_pair(top,adjacent_list[top][i].second);
                if(is_goal(new_node)){
                    goal = new_node;
                    flag = false;
                    break;
                }
                BFS_queue.push(new_node);
            }
        }
    }

}

void make_graph(){
    for(int i=1 ; i<=n ; i++){
        for(int j=1 ; j<=n ; j++){
            int node1 = i*(n+1) + j;
            int node2;
            ///Capitan Rocket move...
            for(int t=1 ; t<=n ; t++){
                if(t!=i){
                    node2 = t*(n+1) + j;
                    if(is_cross(i,t,node_color[j])){
                        adjacent_list[node1].push_back(make_pair(node2,'R'));
                    }
                }
            }
            ///Lieutenant Lucky move...
            for(int t=1 ; t<=n ; t++){
                if(t!=j){
                    node2 = i*(n+1) + t;
                    if(is_cross(j,t,node_color[i])){
                        adjacent_list[node1].push_back(make_pair(node2,'L'));
                    }
                }
            }
        }
    }
}

void input(){
    std::fstream myfile("input_maze.txt", std::ios_base::in);

    myfile>>n>>m;
    for(int i=1 ; i<n ; i++){
        myfile>>node_color[i];
    }
    node_color[n] = 'N';
    myfile>>st1>>st2;
    for(int i=0 ; i<m ; i++){
        myfile>>edges[i].s>>edges[i].f>>edges[i].c;
        primary_edge_node[edges[i].s].push_back(i);
    }
}

void output(){
    vector<pair<char,int> > output_traceback;
    int last = goal;
    if(last==0){
        cout<<"There is not path!\n";
        return;
    }
    while(last!=st1*(n+1) + st2){
        int new_first = parent[last].second;
        int new_second;
        if(parent[last].second=='R'){
            new_second = last/(n+1);
        }
        else{
            new_second = last%(n+1);
        }
        last = parent[last].first;
        output_traceback.push_back(make_pair(new_first,new_second));
    }

    while(output_traceback.size()){
        pair<char,int> pci = output_traceback[output_traceback.size()-1];
        output_traceback.pop_back();
        cout<<pci.first<<" "<<pci.second<<endl;
    }
}

int main(){
    input();
    make_graph();
    BFS();
    output();

    return 0;
}
